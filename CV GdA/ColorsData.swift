import Foundation
import UIKit

struct DrawingColors {
    // Navigation bar colors
    static let NavigationBarBackgroundColor = UIColor(red: 46, green: 148, blue: 255)
    static let NavigationBarTitleColor = UIColor.white
    
    // Tab bar colors
    static let DefaultTabBarBackgroundColor = UIColor(red: 249, green: 249, blue: 249)
    static let SelectedIconTextColor = UIColor(red: 46, green: 148, blue: 255)
    static let UnselectedIconTextColor = UIColor(red: 141, green: 140, blue: 146)
    
    // Chat colors
    static let ChatOtherColor = UIColor(red: 229, green: 229, blue: 234)
    
    
}
