//
//  ContactsViewController.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 21/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit
import MessageUI
import Contacts
import ContactsUI

private let myPhoneNumber = "073 581 35 18"
private let myEmail = "dapolito.work@gmail.com"
private let myWebpage = "www.linkedin.com/in/giuseppedapolito/"

class ContactsViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var myPhoto: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func callButtonTouched(_ sender: UIButton) {
        callGiuseppe()
    }
    @IBAction func messageButtonTouched(_ sender: UIButton) {
        sendMessage()
    }
    @IBAction func mailButtonTouched(_ sender: UIButton) {
        sendMail()
    }
    
    @IBAction func exploreButtonTouched(_ sender: UIButton) {
        openWebpage()
    }
    
    @IBAction func addToContactsButtonTouched(_ sender: UIButton) {
        addAsContact()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set up title and icon of this controller
        title = "Contacts"
        tabBarItem = UITabBarItem(title: "Contacts", image: UIImage(named: "ContactsIconUnselected"), selectedImage: UIImage(named: "ContactsIconSelected"))
        navigationController?.navigationBar.barStyle = .black   // Sets status bar with white text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Rounds the profile photo
        view.layoutIfNeeded()
        myPhoto.makeRounded()
        
        // Disables interaction with table view
        tableView.allowsSelection = false
        tableView.isScrollEnabled = false
    }
}

// MARK:- Action buttons implementation
extension ContactsViewController: MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, CNContactViewControllerDelegate {
    
    // MARK:- Call
    func callGiuseppe() {
        // Let the user confirm if he really wants to call me
        let alertController = UIAlertController(title: "Call Giuseppe?", message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Call", style: .default) { (_) in
            self.callMyNumber()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func callMyNumber() {
        let trimmedPhoneNumber = myPhoneNumber.removeWhitespaces()  // Removes white spaces from my phone number
        let phoneURL = URL(string: "tel://\(trimmedPhoneNumber)")!  // Phone numbers treated as URLs
        
        if UIApplication.shared.canOpenURL(phoneURL) {
            UIApplication.shared.open(phoneURL)
        } else {
            displayAlertMessage(title: "Cannot call", message: "A problem occurred. Please check your network coverage.")
        }
    }
    
    // MARK:- Message
    func sendMessage() {
        let messageVC = MFMessageComposeViewController()
        
        if MFMessageComposeViewController.canSendText() {
            // The device can send text messages
            messageVC.recipients = [myPhoneNumber.removeWhitespaces()]
            messageVC.messageComposeDelegate = self
            present(messageVC, animated: true, completion: nil)
            
        } // iOS automatically promps an error when messages can't be sent
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        // Delegate implementation
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Mail
    func sendMail() {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        
        if MFMailComposeViewController.canSendMail() {
            mailVC.setToRecipients( [myEmail] )
            present(mailVC, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- Webpage
    func openWebpage() {
        let webpageToOpen = URL(string: "http://\(myWebpage)")!
        UIApplication.shared.open(webpageToOpen)
    }
    
    // MARK:- Add Contact
    func addAsContact() {
        // Create new Giuseppe's contact and fill in the information
        let contact = CNMutableContact()
        
        // Profile photo
        if let img = UIImage(named: "GiuseppeFoto.png") {
            contact.imageData = UIImagePNGRepresentation(img) as Data?
        }
        
        // Name
        contact.givenName = "Giuseppe"
        contact.familyName = "d Apolito"
        
        // Email
        contact.emailAddresses = [ CNLabeledValue(label: CNLabelWork, value: myEmail as NSString) ]
        
        // Phone
        contact.phoneNumbers = [ CNLabeledValue(label: CNLabelPhoneNumberiPhone, value: CNPhoneNumber(stringValue: myPhoneNumber)) ]
        
        // Webpage
        contact.urlAddresses = [ CNLabeledValue(label: CNLabelURLAddressHomePage, value: myWebpage as NSString) ]
        
        // Present the contact UI
        let store = CNContactStore()
        let contactController = CNContactViewController(forUnknownContact: contact)
        contactController.contactStore = store
        contactController.delegate = self
        navigationController?.pushViewController(contactController, animated: true)
    }
}

// MARK:- Table view DataSource
extension ContactsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "mobile"
            cell.detailTextLabel?.text = myPhoneNumber
        case 1:
            cell.textLabel?.text = "work"
            cell.detailTextLabel?.text = myEmail
        case 2:
            cell.textLabel?.text = "webpage"
            cell.detailTextLabel?.text = myWebpage
        default:
            break
        }
        
        return cell
    }
}
