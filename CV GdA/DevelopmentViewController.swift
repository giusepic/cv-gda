//
//  DevelopmentViewController.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 21/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit

class DevelopmentViewController: UIViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set up title and icon of this controller
        title = "Development"
        tabBarItem = UITabBarItem(title: "Development", image: UIImage(named: "DevelopmentIconUnselected"), selectedImage: UIImage(named: "DevelopmentIconSelected"))
        navigationController?.navigationBar.barStyle = .black   // Sets status bar with white text
    }
}
