//
//  ExperienceCompanyTableViewCell.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 22/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit

class ExperienceCompanyTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var companyAndLocation: UILabel!
    @IBOutlet weak var timePeriod: UILabel!
    @IBOutlet weak var jobDescription: UILabel!
    
    var company: ExperienceCompany? {
        didSet {
            initializeCell()
        }
    }
    
    private func initializeCell() {
        guard let comp = company else { return }
        
        jobTitle.text = comp.jobTitle
        companyAndLocation.text = comp.companyAndLocation
        timePeriod.text = comp.timePeriod
        jobDescription.text = comp.experienceDescription
    }

}
