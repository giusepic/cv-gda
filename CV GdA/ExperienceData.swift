import Foundation

struct ExperienceCompany {
    var jobTitle: String
    var companyAndLocation: String
    var timePeriod: String
    var experienceDescription: String
}

let experienceCompanies: [ExperienceCompany] = [ExperienceCompany(jobTitle: "Project manager",
                                                                  companyAndLocation: "Gleechi AB, Stockholm.",
                                                                  timePeriod: "2016 - Current",
                                                                  experienceDescription: "After being granted 500.000 SEK during my master thesis, I am now leading the development of a virtual reality environment for stroke rehabilitation. My main duties include accurate market research, stakeholders’ involvement and coordination of programmers."),
                                                ExperienceCompany(jobTitle: "iOS developer",
                                                                  companyAndLocation: "Self-employed, Stockholm.",
                                                                  timePeriod: "2015 - Current",
                                                                  experienceDescription: "I developed the MVP for two startups, which were successfully used for pitches and proof of concept. I am also working on a healthcare marketplace app that was awarded in the latest Tieto’s hackathon."),
                                                ExperienceCompany(jobTitle: "Vice president",
                                                                  companyAndLocation: "International Committee, Karolinska Institutet.",
                                                                  timePeriod: "2014 - 2015",
                                                                  experienceDescription: "Plan, recruit students and execute different projects aimed at better internationalization of Karolinska. I was also in charge of running meetings and ensure the quality of the reports."),
                                                ExperienceCompany(jobTitle: "Organizer and Marketing Manager",
                                                                  companyAndLocation: "World Values Initiative, Stockholm",
                                                                  timePeriod: "2015",
                                                                  experienceDescription: "Part of of the organizing team for the World Values Day 2015, a one-day event supported by several companies including SEB and Oriflame. I was also in charge of social media campaigns, branding and promotion of the event on different channels.")]
