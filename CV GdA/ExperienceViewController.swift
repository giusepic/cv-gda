//
//  PortfolioViewController.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 21/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    
    // MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    
    var companies = [ExperienceCompany]()
    
    // MARK:- VC Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set up title and icon of this controller
        title = "Experience"
        tabBarItem = UITabBarItem(title: "Experience", image: UIImage(named: "ExperienceIconUnselected"), selectedImage: UIImage(named: "ExperienceIconSelected"))
        navigationController?.navigationBar.barStyle = .black   // Sets status bar with white text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        companies = experienceCompanies
        
        // Sets automatic cell height
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.allowsSelection = false
    }
    
}

extension ExperienceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell", for: indexPath) as! ExperienceCompanyTableViewCell
        
        cell.company = companies[indexPath.row]
        
        return cell
    }
}
