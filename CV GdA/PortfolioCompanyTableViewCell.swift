//
//  ExperienceCompanyTableViewCell.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 21/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit

class PortfolioCompanyTableViewCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var workDescription: UILabel!
    
    
    var company: PortfolioProject? {
        didSet {
            initializeCell()
        }
    }
    
    private func initializeCell() {
        guard let comp = company else { return }
        
        logo.image = UIImage(named: comp.logoImageName)
        name.text = comp.name
        workDescription.text = comp.description
    }
    
    /*
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
*/
}
