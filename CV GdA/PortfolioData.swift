import Foundation

struct PortfolioProject {
    var name: String
    var description: String
    var logoImageName: String
}

let enterpriseCompanies: [PortfolioProject] = [PortfolioProject(name: "SourceKid",
                                                                description: "iPad app that allows kids to freely compose a drawing by interacting with vectorial lines and curves.",
                                                                logoImageName: "SourceKidLogo.png"),
                                               PortfolioProject(name: "Mind Music Lab",
                                                                description: "iPhone app that connects to the Sensus smart guitar, downloads the stored audio file into the app and reproduces it.",
                                                                logoImageName: "MindMusicLabLogo.png")]
                                      
let privateCompanies: [PortfolioProject] = [PortfolioProject(name: "Joppi",
                                                             description: "iPhone dating app. The user can chat with people around using Bluetooth for discovery and then, once matched, continue using regular network.",
                                                             logoImageName: "JoppiLogo.png"),
                                            PortfolioProject(name: "Loovia",
                                                             description: "Marketplace for healthcare services, where privates can book and manage appointments with registered care providers.",
                                                             logoImageName: "LooviaLogo.png")]

