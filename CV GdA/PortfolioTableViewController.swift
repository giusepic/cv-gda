//
//  ExperienceTableViewController.swift
//  CV GdA
//
//  Created by Giuseppe d'Apolito on 21/03/17.
//  Copyright © 2017 Giusepic Coding. All rights reserved.
//

import UIKit

class PortfolioTableViewController: UITableViewController {
    
    // MARK:- Variables
    var companies : [String : [PortfolioProject]] = ["ENTERPRISE" : [PortfolioProject](), "PRIVATE" : [PortfolioProject]()]
    
    // MARK:- VC Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set up title and icon of this controller
        title = "Portfolio"
        tabBarItem = UITabBarItem(title: "Portfolio", image: UIImage(named: "PortfolioIconUnselected"), selectedImage: UIImage(named: "PortfolioIconSelected"))
        navigationController?.navigationBar.barStyle = .black   // Sets status bar with white text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateCompanies()
        
        // Sets automatic cell height
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.allowsSelection = false
    }
    
    // MARK:- Methods
    // Populates list of companies
    private func populateCompanies() {
        companies["ENTERPRISE"] = enterpriseCompanies
        companies["PRIVATE"] = privateCompanies
    }

    // MARK:- Table view data source
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard section < companies.count else { return nil }
        return section == 0 ? "ENTERPRISE" : "PRIVATE"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < companies.count else { return 0 }
        return section == 0 ? companies["ENTERPRISE"]!.count : companies["PRIVATE"]!.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return companies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PortfolioCell", for: indexPath) as! PortfolioCompanyTableViewCell
        
        switch indexPath.section {
        case 0:
            let comp = companies["ENTERPRISE"]![indexPath.row]
            cell.company = comp
            
        case 1:
            let comp = companies["PRIVATE"]![indexPath.row]
            cell.company = comp
            
        default:
            break
        }
        
        return cell
    }
}
