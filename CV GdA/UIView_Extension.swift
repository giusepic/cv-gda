import Foundation
import UIKit

extension UIView {
    // Makes the view circular
    func makeRounded() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}
